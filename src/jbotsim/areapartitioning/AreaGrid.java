package jbotsim.areapartitioning;

import java.awt.*;

/**
 * Created by vklein on 10/04/15.
 * grille couvrant la zone à répartir entre les différents agents.
 */
public class AreaGrid implements Cloneable {

    private AreaCell[][] grid = null;
    private int cellSize;
    private int nbColumns, nbRows;

    public AreaGrid(int width, int height, int cellSize) {
        this.cellSize = cellSize;
        this.nbColumns  = (int) Math.ceil((float) width/cellSize);
        this.nbRows     = (int) Math.ceil((float) height/cellSize);
        this.grid       = new AreaCell[nbColumns][nbRows];
        this.cellSize   = cellSize;

        int xcenter, ycenter;
        for(int i=0; i < nbColumns; i++) {
            for(int j=0; j<nbRows; j++) {
                xcenter = i*cellSize + cellSize/2;
                ycenter = j*cellSize + cellSize/2;
                grid[i][j] = new AreaCell(i,j, new Point(xcenter, ycenter));
            }
        }
    }

    public int getCellSize() {
        return cellSize;
    }

    public int getNbColumns() {
        return nbColumns;
    }

    public int getNbRows() {
        return nbRows;
    }

    /**
     *
     * @param column
     * @param row
     * @return
     */
    public AreaCell getCell(int column, int row) {
        return grid[column][row];
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        AreaGrid clone = (AreaGrid) super.clone();
        AreaCell[][] grid = new AreaCell[nbColumns][nbRows];

        for(int i=0; i<nbColumns; i++) {
            for(int j=0; j<nbRows; j++) {
                grid[i][j] = (AreaCell) this.grid[i][j].clone();
            }
        }
        clone.grid = grid;
        return clone;
    }
}
