package jbotsim.areapartitioning;

import jbotsim.Node;

import java.awt.*;
import java.util.UUID;

/**
 * Created by vklein on 15/04/15.
 * Node ayant un agent territorial
 */
public interface TerritorialNode {

    Agent getAgent();
    Node getNode();
    Color getTerritoryColor();
    UUID getId();
    void resizeArea(DynamicArea dynamicArea);
}
