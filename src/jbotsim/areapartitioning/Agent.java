package jbotsim.areapartitioning;

import jbotsim.utils.NeighbourStep;

import java.awt.*;
import java.util.*;

/**
 * Created by vklein on 10/04/15.
 */
public class Agent {

    private static final int DUREE_VIE_VOISINS_DEFAULT = 15000;
    public static int dureeDeVieVoisins = DUREE_VIE_VOISINS_DEFAULT;
    private static Random random = new Random();

    protected HashSet<AreaCell> territory = new HashSet<AreaCell>();
    protected AreaCell centroid;
    protected AreaGrid grid;
    protected UUID id;
    //territorialNode Liée à l'agent
    private TerritorialNode territorialNode;
    //Map des agents voisins et date de leur derniere rencontre en millisecondes
    private HashMap<UUID, Long> neighbourLastContact = new HashMap<UUID, Long>();
    private DynamicArea dynamicArea;

    public Agent(AreaGrid grid, AreaCell centroid, TerritorialNode node) {
        this.id = UUID.randomUUID();
        this.centroid = centroid;
        this.grid = grid;
        if(centroid != null)
            this.territory.add(this.centroid);
        this.territorialNode = node;
    }

    /**
     * Look neighbour cells and decide if they are added to the territory
     */
    public void prospect() {
        int col, row;
        Set<AreaCell> oldTerritory = new HashSet<AreaCell>(this.territory);

        for(AreaCell cell : oldTerritory) {
            //parcours du voisinage
            col = cell.getColumn();
            row = cell.getRow();
            TerritorialNode owner;

            for(NeighbourStep ns : NeighbourStep.values()) {
                if(cellExist(col+ns.getXstep(), row+ns.getYstep())) {
                    AreaCell neighbourCell = this.grid.getCell(col+ns.getXstep(),row+ns.getYstep());
                    if(!this.territory.contains(neighbourCell)) {
                        owner = neighbourCell.getOwner();

                        if(owner != null) {
                            if(winfight(owner, neighbourCell)) {
                                this.conquer(owner, neighbourCell);
                            }
                        } else {
                            this.conquer(null, neighbourCell);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param enemy
     * @param cell
     * @return
     */
    public boolean winfight(TerritorialNode enemy, AreaCell cell ) {
        boolean res = false;
        if(enemy == null || enemy.getAgent() == null) {
            return true;
        }
        int diff = enemy.getAgent().strength(cell) - this.strength(cell);
        if( diff > 0 ) {
            res = true;
        } else if(diff == 0 && this.territory.size() < enemy.getAgent().territory.size()) {
            res = true;
        }
        return res;
    }

    /**
     * @param enemy
     * @param cell
     */
    public void conquer(TerritorialNode enemy, AreaCell cell) {
        if(enemy != null) {
            enemy.getAgent().getTerritory().remove(cell);
        }
        this.territory.add(cell);
        cell.setOwner(territorialNode);
    }

    public void retreat(TerritorialNode enemy, AreaCell cell) {
        cell.setOwner(enemy);
        this.territory.remove(cell);
    }

    /**
     * indique quelle est la force de l'agent dans la cellule en parametres.
     * @param cell
     * @return
     */
    private int strength(AreaCell cell) {
        return (int)centroid.getCenter().distance(cell.getCenter());
    }

    /**
     * Return true if a cell row y, column x exist in the AreaGrid
     * @param x
     * @param y
     * @return
     */
    private boolean cellExist(int x, int y) {
        boolean res = true;

        if(x < 0 || y < 0 || x >= grid.getNbColumns() || y >= grid.getNbRows() ) {
            res = false;
        }

        return res;
    }

    public Set<AreaCell> getTerritory() {
        return this.territory;
    }

    /**
     * Update the centroid after the territory
     */
    public void computeCentroid() {

        int totX = 0;
        int totY = 0;

        for(AreaCell cell : territory) {
            totX += cell.getColumn();
            totY += cell.getRow();
        }

        int xCentroid = (int) Math.floor((float)totX / territory.size());
        int yCentroid = (int) Math.floor((float)totY / territory.size());

        this.centroid = grid.getCell(xCentroid, yCentroid);
    }

    public AreaCell getCentroid() {
        return centroid;
    }

    public AreaGrid getGrid() {
        return grid;
    }

    public UUID getId() {
        return id;
    }

    public HashMap<UUID, Long> getNeighbourLastContact() {
        return neighbourLastContact;
    }

    public void controlNeighbourPresence() {
        for(UUID id : this.neighbourLastContact.keySet()) {
            if((System.currentTimeMillis() - neighbourLastContact.get(id)) >= dureeDeVieVoisins) {
                eraseNeighbourData(id);
            }
        }
    }

    private void eraseNeighbourData(UUID id) {
        //Var temp
        TerritorialNode tnode;
        AreaCell cell;

        for(int col=0; col < grid.getNbColumns(); col++) {
            for(int row=0; row < grid.getNbRows(); row ++) {
                cell = grid.getCell(col, row);
                tnode = cell.getOwner();
                if(tnode != null && tnode.getId() == id) {
                    //La cellule est noté comme appartenant à l'id testé
                    // on met donc son appartenance à null
                    cell.setOwner(null);
                }
            }
        }
    }

    //TODO gestion de l'ancien centroid si cette fonction est appelé alors que l'ancien centroid est non null.
    public void setCentroid(AreaCell centroid) {
        this.centroid = centroid;
        this.territory.add(centroid);
    }

    /**
     * Prend en compte le changement de la zone dans laquelle
     * on peut répartir le territoire.
     * @param dynamicArea
     */
    public void resizeArea(DynamicArea dynamicArea) {
        this.dynamicArea = dynamicArea;
        /*
            Vide le territoire,
         */
        this.territory.clear();
        /*
            Replace le centroid si nécessaire : cad si le centroid courant se trouve en dehors de la
            nouvelle zone définie.
         */
        if(!isAreaCellInDynamicArea(centroid)) {
            // on définie les colonnes et lignes maximum et minimums
            // correspondant à la nouvelle zone.
            int cellSize = grid.getCellSize();
            int colMin = (int) Math.abs(dynamicArea.getTopLeft().getX() / cellSize);
            int colMax = (int) Math.abs(dynamicArea.getBottomRight().getX() / cellSize);
            int rowMin = (int) Math.abs(dynamicArea.getTopLeft().getY() / cellSize);
            int rowMax = (int) Math.abs(dynamicArea.getBottomRight().getY() / cellSize);
            //On replace aléatoirement le centroid en un point se trouvant dans la zone dynamique

            int col = randInt(colMin, colMax);
            int row = randInt(rowMin, rowMax);
            this.centroid = this.grid.getCell(col, row);
        }

    }

    private boolean isAreaCellInDynamicArea(AreaCell areaCell) {
        boolean res = false;
        if(dynamicArea != null) {
            Point point = new Point(areaCell.getColumn() * grid.getCellSize(), areaCell.getRow() * grid.getCellSize());
            Point topLeft = dynamicArea.getTopLeft();
            Point bottomRight = dynamicArea.getBottomRight();

            if(point.getX() >= topLeft.getX() && point.getY() >= topLeft.getY() &
                    point.getX() <= bottomRight.getX() && point.getY() <= bottomRight.getY()) {
                res = true;
            }
        } else {
            res = true;
        }

        return res;
    }

    private static int randInt(int min, int max) {
        int nbElem = max - min;
        return random.nextInt(nbElem) + min;
    }
}
