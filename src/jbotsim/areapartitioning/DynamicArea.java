package jbotsim.areapartitioning;

import java.awt.*;

/**
 * Created by vklein on 22/06/15.
 * Zone partielle sur laquelle s'exerce l'algorithme de répartition de territoire
 * = Un rectangle dans le territoire total, il est défini d'une part par son point en haut à gauche d'une part et en bas
 * à droite d'autre part.
 */
public class DynamicArea {
    private Point topLeft;
    private Point bottomRight;

    public DynamicArea(Point topLeft, Point bottomRight) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public Point getTopLeft() {
        return topLeft;
    }
}
