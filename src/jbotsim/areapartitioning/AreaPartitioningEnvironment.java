package jbotsim.areapartitioning;

import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.daisie.DaisieTopology;
import jbotsim.daisie.node.Wing;
import jbotsim.event.TopologyListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vklein on 15/04/15.
 */
public class AreaPartitioningEnvironment implements TopologyListener {

    private DaisieTopology topology;
    private List<TerritorialNode> listTerritorialNode = new ArrayList<TerritorialNode>();
    private AreaGrid grid = null;
    //Zone actuelle de répartition de territoire, si null toute la surface disponible est utilisée.
    private DynamicArea currentArea;

    public AreaPartitioningEnvironment(int cellSize, DaisieTopology topology) {
        this.topology = topology;
        int width = (int)topology.getDimensions().getWidth();
        int height = (int)topology.getDimensions().getHeight();

        this.grid = new AreaGrid(width, height, cellSize);
        this.topology.setGrid(grid);
        this.topology.addTopologyListener(this);
    }



    public Topology getTopology() {
        return topology;
    }

    /**
     * Round de réévaluation des territoires
     */
    public void runConquest() {
        for(TerritorialNode node : listTerritorialNode) {
            node.getAgent().prospect();
        }

        for(TerritorialNode node : listTerritorialNode) {
            node.getAgent().computeCentroid();
        }
    }

    public AreaGrid getGrid() {
        return grid;
    }

    public List<TerritorialNode> getListTerritorialNode() {
        return listTerritorialNode;
    }

    @Override
    public void onNodeAdded(Node node) {
        if(node instanceof Wing) {
            this.listTerritorialNode.add((Wing)node);
        }
    }

    @Override
    public void onNodeRemoved(Node node) {
        if(node instanceof Wing) {
            this.listTerritorialNode.remove(node);
        }
    }

    public DynamicArea getCurrentArea() {
        return currentArea;
    }

    public void setCurrentArea(DynamicArea currentArea) {
        this.currentArea = currentArea;
        for(TerritorialNode tn : listTerritorialNode) {
            tn.resizeArea(currentArea);
        }
    }
}
