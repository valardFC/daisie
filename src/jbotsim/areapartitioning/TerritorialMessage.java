package jbotsim.areapartitioning;

import java.awt.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by vklein on 28/04/15.
 */
public class TerritorialMessage {
    private Set<AreaCell> territory;
    private Color agentColor;
    private UUID id;
    private AreaCell centroid;

    public TerritorialMessage(UUID agentID, Color agentColor, Set<AreaCell> territory, AreaCell centroid) {
        this.agentColor = agentColor;
        this.territory = territory;
    }

    public Color getAgentColor() {
        return agentColor;
    }

    public void setAgentColor(Color agentColor) {
        this.agentColor = agentColor;
    }

    public Set<AreaCell> getTerritory() {
        return territory;
    }

    public void setTerritory(Set<AreaCell> territory) {
        this.territory = territory;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public AreaCell getCentroid() {
        return centroid;
    }

    public void setCentroid(AreaCell centroid) {
        this.centroid = centroid;
    }
}
