package jbotsim.areapartitioning;

import java.awt.*;

/**
 * Created by vklein on 10/04/15.
 */
public class AreaCell implements Cloneable {

    //Propriétés
    private int column; //Cell column
    private int row; //Cell row
    //center x,y of the cell
    private Point center;
    private TerritorialNode owner;

    public AreaCell(int column, int row, Point point) {
        this.column = column;
        this.row = row;
        this.center = point;
    }

//     Distance entre la cellule courante et celle en paramètres
//     en considérant que le déplacement en diagonales est possible
    public int distance(AreaCell cell) {
        int fx = Math.abs(this.column - cell.column);
        int fy = Math.abs(this.row - cell.row);

        return Math.max(fx, fy);
    }

    public TerritorialNode getOwner() {
        return owner;
    }

    public void setOwner(TerritorialNode owner) {
        this.owner = owner;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Point getCenter() {
        return center;
    }
}
