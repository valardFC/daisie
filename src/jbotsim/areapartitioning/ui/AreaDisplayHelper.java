package jbotsim.areapartitioning.ui;

import jbotsim.Node;
import jbotsim.areapartitioning.*;
import jbotsim.event.SelectionListener;

import java.awt.*;
import java.util.Set;

/**
 * Created by vklein on 15/04/15.
 */
public class AreaDisplayHelper implements SelectionListener {

    private static Color black = new Color(0, 0, 0, 60);
    private AreaPartitioningEnvironment environment = null;
    private boolean showTerritories = false;
    private TerritorialNode nodeMapToDisplay = null;

    public AreaDisplayHelper(AreaPartitioningEnvironment environment) {
        this.environment = environment;
        this.environment.getTopology().addSelectionListener(this);
    }


    public void paint(Graphics graphics) {
        if(nodeMapToDisplay == null) {
            //Affichage des territoires
            for (TerritorialNode node : this.environment.getListTerritorialNode()) {
                paintTerritory(node, graphics);
            }
        } else {
            //List<AreaCell> listCell = this.environment.getGrid().;
            AreaCell cell = null;
            AreaGrid areaGrid = nodeMapToDisplay.getAgent().getGrid();
            TerritorialNode owner;
            Color oldColor = graphics.getColor();
            int cellSize = areaGrid.getCellSize();

            for(int i=0; i < areaGrid.getNbColumns(); i++)  {
                for(int j=0; j < areaGrid.getNbRows(); j++) {
                    cell = areaGrid.getCell(i, j);
                    owner = cell.getOwner();
                    if(owner != null ) {
                        graphics.setColor(owner.getTerritoryColor());
                        graphics.fillRect(cellSize * i, cellSize * j, cellSize, cellSize);
                    }
                }
            }

            graphics.setColor(black);
            AreaCell centroid = nodeMapToDisplay.getAgent().getCentroid();
            graphics.fillRect(cellSize*centroid.getColumn(), cellSize*centroid.getRow(), cellSize, cellSize);

            graphics.setColor(oldColor);
        }
    }

    /**
     * Graphical display of an agent
     * @param tnode
     * @param graphics
     */
    private void paintTerritory(TerritorialNode tnode, Graphics graphics) {
        Agent agent = tnode.getAgent();
        int column, row;
        int cellSize = environment.getGrid().getCellSize();
        Set<AreaCell> list = agent.getTerritory();
        Color oldColor = graphics.getColor();

        graphics.setColor(tnode.getTerritoryColor());

        for(AreaCell cell : list) {
            column = cell.getColumn();
            row = cell.getRow();
            graphics.fillRect(cellSize*column, cellSize*row, cellSize, cellSize);
        }

        graphics.setColor(black);
        AreaCell centroid = agent.getCentroid();
        graphics.fillRect(cellSize*centroid.getColumn(), cellSize*centroid.getRow(), cellSize, cellSize);

        graphics.setColor(oldColor);
    }

    public boolean isShowTerritories() {
        return showTerritories;
    }

    public void setShowTerritories(boolean showTerritories) {
        this.showTerritories = showTerritories;
    }

    @Override
    public void onSelection(Node node) {
        if(node instanceof TerritorialNode) {
            TerritorialNode tnode = (TerritorialNode) node;
            if(this.environment.getListTerritorialNode().contains(tnode)) {
                if(this.nodeMapToDisplay == tnode) {
                    this.nodeMapToDisplay = null;
                } else {
                    this.nodeMapToDisplay = tnode;
                }
            }
        }
    }
}
