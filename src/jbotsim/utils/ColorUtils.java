package jbotsim.utils;

import java.awt.*;
import java.util.Random;

/**
 * Created by vklein on 15/04/15.
 */
public class ColorUtils {
    private static Random random = new Random();

    public static Color getRandomHSBColor() {
        float hue = (float)random.nextInt(360);
        float sat = (float)(random.nextInt(40)+60);
        float bri = (float)(random.nextInt(40)+60);
        return Color.getHSBColor(hue, sat, bri);
    }

    public static Color getRandomColor() {
        return new Color(random.nextFloat(), random.nextFloat(), random.nextFloat());
    }
}
