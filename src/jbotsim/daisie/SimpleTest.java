package jbotsim.daisie;

import jbotsim.Topology;
import jbotsim.daisie.node.SimpleWing2;
import jbotsim.ui.JTopology;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 02/12/15.
 */
public class SimpleTest {

    public static void main(String [] argv) {

        Topology topology = new Topology(400, 400);
        topology.setNodeModel("simpleDrone", SimpleWing2.class);
        JTopology jTopology = new JTopology(topology);

        new JViewer(jTopology);
    }
}
