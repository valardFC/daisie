package jbotsim.daisie.node;

/**
 * Created by vklein on 06/03/15.
 */
public class BlueHatGnome extends GardenGnome {
    public BlueHatGnome() {
        super();
        setProperty("icon", "/jobtsim/daisie/resources/gnome-bluehat.png");
    }
}
