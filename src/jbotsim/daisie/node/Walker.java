package jbotsim.daisie.node;

import jbotsim.Node;
import jbotsim.evap.EvapNode;
import jbotsim.evap.PheromoneCell;
import jbotsim.evap.PheromoneGrid;

import java.awt.*;

/**
 * Created by vklein on 05/03/15.
 */
public class Walker extends EvapNode { //EvapNode {

    public static final String MODEL_NAME = "walker";
    public Walker() {
        super();
        setIcon("/jbotsim/daisie/resources/robot.png");
        setSensingRange(42);
        setCommunicationRange(400);
    }

    public void onClock(){
        super.onClock();
        for (Node n : getSensedObjects()) {
            if (n.getClass() == RedHatGnome.class) {
                n.setColor(Color.GREEN);
            }
        }
    }
}
