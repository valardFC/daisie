package jbotsim.daisie.node;

/**
 * Created by vklein on 06/03/15.
 */
public class RedHatGnome extends GardenGnome {
    public RedHatGnome() {
        super();
        setProperty("icon", "/jbotsim/daisie/resources/gnome-redhat.png");
    }
}
