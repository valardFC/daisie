package jbotsim.daisie.node;

import jbotsim.Node;

/**
 * Created by vklein on 30/06/15.
 */
public class SimpleWalker extends Node {
    private static final int COMMUNICATION_RANGE = 0;
    public static final String MODEL_NAME = "Walker";

    public SimpleWalker() {
        super();
        setIcon("/jbotsim/daisie/resources/robot.png");
        setSensingRange(0);
        disableWireless();
    }
}
