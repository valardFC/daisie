package jbotsim.daisie.node;

import jbotsim.Message;
import jbotsim.Node;
import jbotsim.areapartitioning.*;
import jbotsim.utils.ColorUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * Created by vklein on 05/03/15.
 */
public class Wing extends Node implements TerritorialNode {
    //Distance traveled by mouvement
    public static final double DEFAULT_STEP = 7;
    public static final int COMMUNICATION_RANGE = 2000;
    public static final String MODEL_NAME = "wing";

    //Class properties
    private Random random = new Random();

    //Instance properties
    private Agent agent;
    private Point waypoint;
    private Color territoryColor;

    //Dimension of the mission area
    private double maxMove = DEFAULT_STEP;

    public Wing() {
        super();
        this.setCommunicationRange(COMMUNICATION_RANGE);
    }

    public Wing(AreaGrid grid) {
        this(grid, null, ColorUtils.getRandomColor());
    }

    public Wing(AreaGrid grid, AreaCell centroid, Color color) {
        super();
        this.setCommunicationRange(COMMUNICATION_RANGE);
        random = new Random();
        this.setColor(color);
        try {
            this.agent = new Agent((AreaGrid)grid.clone(), centroid, this);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        setIcon("/jbotsim/daisie/resources/uav.png");
        setSize(20);
        this.territoryColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), 30);
    }

    /**
     * Sert dans le cas ou la node a été ajoutée via l'IHM,
     * Il faut renseigner le centroid de l'agent
     */
    @Override
    public void onStart() {
        if(agent.getCentroid() == null) {
            AreaGrid grid = agent.getGrid();
            int col = (int) Math.abs(getX() / (float) grid.getCellSize());
            int row = (int) Math.abs(getY() / (float) grid.getCellSize());
            agent.setCentroid(grid.getCell(col, row));
            start();
        }
    }

    @Override
    public void onClock() {
        this.agent.computeCentroid();
        this.agent.prospect();
        if(waypoint!= null) {
            double toDest  = distance(waypoint);
            if (toDest <= maxMove ) {
                this.move(toDest);
                //waypoint reached compute new wp
                computeWayPoint();
            } else {
                this.move(maxMove);
            }
        }
        //On vérifie si les voisins sont toujours présents
        this.agent.controlNeighbourPresence();
        sendMap();
    }

    public void goToCentroid() {
        this.waypoint = this.agent.getCentroid().getCenter();
        this.setDirection(this.waypoint);
    }

    @Override
    public Agent getAgent() {
        return this.agent;
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public Color getTerritoryColor() {
        return this.territoryColor;
    }

    @Override
    public UUID getId() {
        return this.agent.getId();
    }

    @Override
    public void onMessage(Message message) {
        super.onMessage(message);

        if(message.getSender() instanceof  TerritorialNode) {
            TerritorialNode sender = (TerritorialNode) message.getSender();
            //On rafraichit la date de dernière rencontre avec l'agent envoyant le message
            this.agent.getNeighbourLastContact().put(sender.getId(), System.currentTimeMillis());

            Object content = message.getContent();

            if (content instanceof TerritorialMessage) {
                mergeAreaMap((TerritorialMessage) content);

            }
        }
    }

    private void computeWayPoint() {
        //Meth1 On tire un point au hasard puis on vérifie qu'il se trouve dans
        // le territoire du drone, si il est en dehors on refait le tir
        //jusqu'à trouver un point dans le territoire
        // Meth2 Non .... on tire une cellule au hasard du territoire et on prend son centre comme waypoint.
        Set<AreaCell> territory = this.agent.getTerritory();
        int idx = random.nextInt(territory.size());

        //Il est nettement moins couteux de créer un ArrayList pour cette opération,
        //que de fonctionner en permanence sur ArrayList à la place d'un HashSet.
        AreaCell destinationCell = new ArrayList<AreaCell>(territory).get(idx);
        this.waypoint = destinationCell.getCenter();
        this.setDirection(this.waypoint);
    }

    public void start() {
        computeWayPoint();
    }

    public void stop() {
        this.waypoint = null;
    }

    private void mergeAreaMap(TerritorialMessage message) {
        //On signale le territoire envoyé dans notre carte locale
        Set<AreaCell> territory = message.getTerritory();
        int col, row;
        AreaCell myCell;


        for(AreaCell cell : territory) {
            col = cell.getColumn();
            row = cell.getRow();
            //On met l'objet Agent comme identifiant, à voir si il ne faudra pas
            // mieux passer par un UUID
            myCell = this.agent.getGrid().getCell(col, row);
            //On teste si la cellule appartient au territoire courant
            if(myCell.getOwner() == this) {
                //Dans ce cas il faut déterminer, qui va gagner la cellule.
                if(this.agent.winfight(cell.getOwner(), myCell)) {
                    this.agent.conquer(cell.getOwner(), myCell);
                } else {
                    this.agent.retreat(cell.getOwner(), myCell);
                }
            } else {
                this.agent.getGrid().getCell(col, row).setOwner(cell.getOwner());
            }
        }
    }

    /**
     * Share territory with neighbour
     */
    private void sendMap() {
        TerritorialMessage content = new TerritorialMessage(
                this.agent.getId(),
                getColor(),
                this.agent.getTerritory(),
                this.agent.getCentroid());

        sendAll(new Message(content));
    }

    @Override
    public void resizeArea(DynamicArea dynamicArea) {
        //A placer dans la classe Agent
        // vider le territoire => classe agent
        //Replacer le centroid si necessaire

    }
}
