package jbotsim.daisie.node;

import jbotsim.Node;

/**
 * Created by vklein on 30/06/15.
 */
public class SimpleWing extends Node {
    private static final int COMMUNICATION_RANGE = 2000;
    public static final String MODEL_NAME = "Wing";

    public SimpleWing() {
        super();
        this.setCommunicationRange(COMMUNICATION_RANGE);
        setIcon("/jbotsim/daisie/resources/uav.png");
        setSize(20);
    }
}
