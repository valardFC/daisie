package jbotsim.daisie.node;

import jbotsim.Node;

/**
 * Created by vklein on 30/06/15.
 */
public class SimpleWing2 extends Node {
    private static final int COMMUNICATION_RANGE = 2000;
    public static final String MODEL_NAME = "Wing";

    public SimpleWing2() {
        super();
        this.setCommunicationRange(0);
        this.setSensingRange(50);
        //setIcon("/jbotsim/daisie/resources/uav.png");
    }
}
