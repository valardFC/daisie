package jbotsim.daisie;

import jbotsim.Topology;
import jbotsim.daisie.node.ControlStation;
import jbotsim.daisie.node.RedHatGnome;
import jbotsim.daisie.node.Walker;
import jbotsim.daisie.node.Wing;
import jbotsim.evap.EvapEnvironment;
import jbotsim.evap.ui.JEvapTopology;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 06/03/15.
 */
public class ScenarioOne {
    public static void main(String [] argv) {

        Topology topology = new Topology(800, 600);
        topology.setNodeModel("walker", Walker.class);
        topology.setNodeModel("drone", Wing.class);
        topology.setNodeModel("gnome", RedHatGnome.class);
        topology.setNodeModel("ControlStation", ControlStation.class);
        EvapEnvironment env = new EvapEnvironment(60, topology, (float)0.0005);
        env.setColoredPheromone(true);
        topology.setClockSpeed(40);

        JEvapTopology jEvapTopology = new JEvapTopology(env);
        jEvapTopology.setDisplayPheromoneAsNumeric(false);

        new JViewer(jEvapTopology);
    }
}
