package jbotsim.daisie;

import jbotsim.Topology;
import jbotsim.daisie.node.SimpleWalker;
import jbotsim.daisie.node.SimpleWing;
import jbotsim.ui.JViewer;

/**
 * Created by vklein on 30/06/15.
 */
public class ImagesPOI {

    public static void main(String [] argv) {
        Topology topology = new Topology();
        topology.setNodeModel(SimpleWalker.MODEL_NAME, SimpleWalker.class);
        topology.setNodeModel(SimpleWing.MODEL_NAME, SimpleWing.class);
        JViewer jViewer = new JViewer(topology);
    }
}
