package jbotsim.daisie.ui;

import jbotsim.areapartitioning.AreaPartitioningEnvironment;
import jbotsim.areapartitioning.ui.AreaDisplayHelper;
import jbotsim.evap.EvapEnvironment;
import jbotsim.evap.ui.EvapDisplayHelper;
import jbotsim.event.ClockListener;
import jbotsim.ui.CommandListener;
import jbotsim.ui.JTopology;

import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by vklein on 15/04/15.
 */
public class JDaisieTopology extends JTopology implements ClockListener, CommandListener {

    //Constantes
    private static final String CMD_SHOWHIDE_LOCAL_MAPS = "Show/Hide local maps";
    private static final String CMD_SHOWHIDE_PHEROMONE = "Show/Hide Pheromones";
    private static final String CMD_DISPLAYMODE_PHEROMONE = "Switch pheromones display mode";
    private static final String CMD_DISPLAYMODE_TERRITORIES = "Territory display mode";

    private EvapEnvironment evapEnvironment = null;
    private AreaPartitioningEnvironment areaEnvironment = null;
    private EvapDisplayHelper evapDisplay = null;
    private AreaDisplayHelper areaDisplay = null;


    public JDaisieTopology(EvapEnvironment environment, AreaPartitioningEnvironment areaEnvironment) {
        super(environment.getTopology());
        this.evapEnvironment = environment;
        this.areaEnvironment = areaEnvironment;
        this.evapDisplay = new EvapDisplayHelper(this, environment);
        this.areaDisplay = new AreaDisplayHelper(areaEnvironment);
        this.topo.addClockListener(this);
        this.addCommand(CMD_SHOWHIDE_LOCAL_MAPS);
        this.addCommand(CMD_SHOWHIDE_PHEROMONE);
        this.addCommand(CMD_DISPLAYMODE_PHEROMONE);
        this.addCommand(CMD_DISPLAYMODE_TERRITORIES);
        this.addCommandListener(this);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        if(isShowPheromone()) {
            evapDisplay.paint(graphics);
        } else if(isShowTerritories()) {
            areaDisplay.paint(graphics);
        }
    }

    public EvapEnvironment getEnvironment() {
        return evapDisplay.getEnvironment();
    }

    public boolean isShowPheromone() {
        return evapDisplay.isShowPheromone();
    }

    public void setShowPheromone(boolean showPheromone) {
        evapDisplay.setShowPheromone(showPheromone);
    }

    public boolean isDisplayPheromoneAsNumeric() {
        return evapDisplay.isDisplayPheromoneAsNumeric();
    }

    public void setDisplayPheromoneAsNumeric(boolean displayPheromoneAsNumeric) {
        evapDisplay.setDisplayPheromoneAsNumeric(displayPheromoneAsNumeric);
    }

    public void setShowTerritories(boolean showTerritories) {
        areaDisplay.setShowTerritories(showTerritories);
    }

    public boolean isShowTerritories() {
        return areaDisplay.isShowTerritories();
    }

    @Override
    public void onClock() {
        updateUI();
    }

    @Override
    public void onCommand(String cmd) {
        if(CMD_SHOWHIDE_LOCAL_MAPS.equals(cmd)) {
            evapEnvironment.setColoredPheromone(!evapEnvironment.isColoredPheromone());
        } else if(CMD_SHOWHIDE_PHEROMONE.equals(cmd)) {
            setShowTerritories(false);
            setShowPheromone(!isShowPheromone());
        } else if(CMD_DISPLAYMODE_PHEROMONE.equals(cmd)) {
            setDisplayPheromoneAsNumeric(!isDisplayPheromoneAsNumeric());
        } else if(CMD_DISPLAYMODE_TERRITORIES.equals(cmd)) {
            setShowPheromone(false);
            setShowTerritories(!isShowTerritories());
        }
        updateUI();
    }
}
