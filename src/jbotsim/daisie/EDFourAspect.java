package jbotsim.daisie;

import jbotsim.Topology;
import jbotsim.areapartitioning.Agent;
import jbotsim.areapartitioning.AreaCell;
import jbotsim.areapartitioning.AreaGrid;
import jbotsim.areapartitioning.AreaPartitioningEnvironment;
import jbotsim.daisie.node.ControlStation;
import jbotsim.daisie.node.RedHatGnome;
import jbotsim.daisie.node.Walker;
import jbotsim.daisie.node.Wing;
import jbotsim.evap.EvapEnvironment;
import jbotsim.daisie.ui.JDaisieTopology;
import jbotsim.ui.JViewer;
import jbotsim.utils.ColorUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 14/04/15.
 */
public class EDFourAspect {
    private static Random random = new Random();
    private static List<Color> lcolors = new ArrayList<Color>();
    private static List<Wing> swarm = new ArrayList<Wing>();
    private static int width = 1200;
    private static int height = 800;

    static {
        lcolors.add(Color.red);
        lcolors.add(Color.green);
        lcolors.add(Color.blue);
        lcolors.add(Color.yellow);
    }



    public static void main(String [] argv) throws CloneNotSupportedException {
        float evapPhStep = (float)0.0005;
        int cellSize = 5;
        int nbDrone = 8;

        DaisieTopology topology = new DaisieTopology(width, height);
        topology.setNodeModel(Walker.MODEL_NAME, Walker.class);
        topology.setNodeModel(Wing.MODEL_NAME, Wing.class);
        topology.setNodeModel("gnome", RedHatGnome.class);
        topology.setNodeModel("ControlStation", ControlStation.class);
        topology.setClockSpeed(40);
        EvapEnvironment env = new EvapEnvironment(60, topology, evapPhStep);
        AreaPartitioningEnvironment areaEnv = new AreaPartitioningEnvironment(cellSize, topology);

        JDaisieTopology jDaisieTopology = new JDaisieTopology(env, areaEnv);
        jDaisieTopology.setShowPheromone(false);
        jDaisieTopology.setShowTerritories(true);

        //Init drone area aware
        AreaGrid grid = areaEnv.getGrid();

        AreaCell centroid = null;
        Iterator<Color> icolor = lcolors.iterator();

        int x,y;
        Wing wing;
        Color c;

        for(int i=0; i<nbDrone; i++) {
            if(icolor.hasNext())
                c = icolor.next();
            else
                c = ColorUtils.getRandomColor();

            wing = createWing((AreaGrid) grid.clone(), c);
            centroid = wing.getAgent().getCentroid();
            x = centroid.getColumn() * cellSize + cellSize/2;
            y = centroid.getRow() * cellSize + cellSize/2;
            topology.addNode(x,y, wing);
            swarm.add(wing);
        }

        for(Wing w2: swarm) {
            w2.start();
        }
        new JViewer(jDaisieTopology);
    }

    private static Wing createWing(AreaGrid grid, Color color) {
        //Select a random cell
        int col = random.nextInt(grid.getNbColumns());
        int row = random.nextInt(grid.getNbRows());
        AreaCell centroid = grid.getCell(col, row);
        return new Wing(grid, centroid, color);
    }

    private static Wing createAndPlaceWing(AreaGrid grid, Color color, int x, int y, Topology topology, int cellSize) {
        int col = Math.abs(x / cellSize);
        int row = Math.abs(y / cellSize);
        AreaCell centroid = grid.getCell(col, row);
        Wing res = new Wing(grid, centroid, color);
        topology.addNode(x, y, res);
        return res;
    }

    private static void fillSquareTerritory(Agent agent, int firstCol, int firstRow, int lastCol, int lastRow) {
//        AreaGrid grid = agent.getGrid();
//        for(int i = firstCol; i< lastCol; i++) {
//            for(int j=firstRow; j < lastRow; j++) {
//                agent.conquer(null, grid.getCell(i,j));
//            }
//        }
    }
}
