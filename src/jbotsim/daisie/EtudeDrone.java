package jbotsim.daisie;

import jbotsim.Topology;
import jbotsim.areapartitioning.Agent;
import jbotsim.areapartitioning.AreaCell;
import jbotsim.areapartitioning.AreaGrid;
import jbotsim.areapartitioning.AreaPartitioningEnvironment;
import jbotsim.daisie.node.ControlStation;
import jbotsim.daisie.node.RedHatGnome;
import jbotsim.daisie.node.Walker;
import jbotsim.daisie.node.Wing;
import jbotsim.daisie.ui.JDaisieTopology;
import jbotsim.evap.EvapEnvironment;
import jbotsim.ui.JViewer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 14/04/15.
 */
public class EtudeDrone {
    private static Random random = new Random();
    private static List<Color> lcolors = new ArrayList<Color>();
    private static List<Wing> swarm = new ArrayList<Wing>();
    private static int width = 1200;
    private static int height = 800;

    static {
        lcolors.add(Color.red);
        lcolors.add(Color.green);
        lcolors.add(Color.blue);
        lcolors.add(Color.yellow);
    }



    public static void main(String [] argv) throws CloneNotSupportedException {
        float evapPhStep = (float)0.0005;
        int cellSize = 15;

        DaisieTopology topology = new DaisieTopology(width, height);
        topology.setClockSpeed(40);
        topology.setNodeModel("walker", Walker.class);
        topology.setNodeModel("drone", Wing.class);
        topology.setNodeModel("gnome", RedHatGnome.class);
        topology.setNodeModel("ControlStation", ControlStation.class);
        EvapEnvironment env = new EvapEnvironment(60, topology, evapPhStep);
        AreaPartitioningEnvironment areaEnv = new AreaPartitioningEnvironment(cellSize, topology);

        JDaisieTopology jDaisieTopology = new JDaisieTopology(env, areaEnv);
        jDaisieTopology.setShowPheromone(false);
        jDaisieTopology.setShowTerritories(true);

        //Init drone area aware
        AreaGrid grid = areaEnv.getGrid();

        AreaCell centroid = null;
        Iterator<Color> icolor = lcolors.iterator();
        int x,y;

        int halfcol = grid.getNbColumns() / 2;
        int halfrow = grid.getNbRows() / 2;

        x = (int) Math.round((double)width  * 0.25);
        y = (int) Math.round((double)height  * 0.25);
        Wing w = createAndPlaceWing(grid, icolor.next(), x, y, topology, cellSize);
        swarm.add(w);
        fillSquareTerritory(w.getAgent(),0, 0, halfcol, halfrow);

        x = (int) Math.round((double)width  * 0.75);
        y = (int) Math.round((double)height  * 0.25);
        w = createAndPlaceWing(grid, icolor.next(), x, y, topology, cellSize);
        swarm.add(w);
        fillSquareTerritory(w.getAgent(), halfcol, 0, grid.getNbColumns(), halfrow);

        x = (int) Math.round((double)width  * 0.25);
        y = (int) Math.round((double)height  * 0.75);
        w = createAndPlaceWing(grid, icolor.next(), x, y, topology, cellSize);
        swarm.add(w);
        fillSquareTerritory(w.getAgent(), 0, halfrow, halfcol, grid.getNbRows());

        x = (int) Math.round((double)width  * 0.75);
        y = (int) Math.round((double)height  * 0.75);
        w = createAndPlaceWing(grid, icolor.next(), x, y, topology, cellSize);
        swarm.add(w);
        fillSquareTerritory(w.getAgent(), halfcol, halfrow, grid.getNbColumns(), grid.getNbRows());

        for(Wing wing: swarm) {
            wing.start();
        }

        new JViewer(jDaisieTopology);
    }

    private static Wing createWing(AreaGrid grid, Color color) {
        //Select a random cell
        int col = random.nextInt(grid.getNbColumns());
        int row = random.nextInt(grid.getNbRows());
        AreaCell centroid = grid.getCell(col, row);
        return new Wing(grid, centroid, color);
    }

    private static Wing createAndPlaceWing(AreaGrid grid, Color color, int x, int y, Topology topology, int cellSize) {
        int col = Math.abs(x / cellSize);
        int row = Math.abs(y / cellSize);
        AreaCell centroid = grid.getCell(col, row);
        Wing res = new Wing(grid, centroid, color);
        topology.addNode(x, y, res);
        return res;
    }

    private static void fillSquareTerritory(Agent agent, int firstCol, int firstRow, int lastCol, int lastRow) {
//        AreaGrid grid = agent.getGrid();
//        for(int i = firstCol; i< lastCol; i++) {
//            for(int j=firstRow; j < lastRow; j++) {
//                agent.conquer(null, grid.getCell(i,j));
//            }
//        }
    }
}
