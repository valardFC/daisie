package jbotsim.daisie;

import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.areapartitioning.AreaGrid;
import jbotsim.daisie.node.Wing;

/**
 * Created by vklein on 26/05/15.
 */
public class DaisieTopology extends Topology{

    private AreaGrid grid;

    public DaisieTopology(int width, int height) {
        super(width, height);
    }

    @Override
    public Node newInstanceOfModel(String s) {
        if(Wing.MODEL_NAME.equals(s)) {
            return new Wing(grid);
        } else {
            return super.newInstanceOfModel(s);
        }
    }

    public AreaGrid getGrid() {
        return grid;
    }

    public void setGrid(AreaGrid grid) {
        this.grid = grid;
    }
}
