package jbotsim.evap.ui;

/**
 * Created by vklein on 15/04/15.
 */

import jbotsim.Node;
import jbotsim.evap.EvapEnvironment;
import jbotsim.evap.EvapNode;
import jbotsim.evap.PheromoneCell;
import jbotsim.evap.PheromoneGrid;
import jbotsim.event.SelectionListener;
import jbotsim.ui.JTopology;
import java.awt.*;

/**
 * Created by vklein on 12/03/15.
 */
public class EvapDisplayHelper implements SelectionListener {
    protected EvapEnvironment environment = null;
    //compte pour chaque cellule le nombre de Node informé qu'il y as des phéromones
    //Sert à afficher les petits carrés de couleurs indiquant quelle Node sait qu'il y
    //a des phéromones dans une cellule donnée.
    private int[][] countNodePhAware = null;
    protected boolean showPheromone = true;
    protected EvapNode nodeMapToDisplay;
    protected JTopology jTopology = null;

    //Pheromone Color
    protected Color phColor = Color.WHITE;
    protected static int phAlphaMax = 160;
    private boolean displayPheromoneAsNumeric = true;

    public EvapDisplayHelper(JTopology jTopology, EvapEnvironment environment) {
        environment.getTopology().addSelectionListener(this);
        this.environment = environment;
        this.jTopology = jTopology;
        countNodePhAware = new int[environment.getGlobalGrid().getNbColumns()][environment.getGlobalGrid().getNbRows()];
    }

    public void paint(Graphics graphics) {
        PheromoneGrid grid = environment.getGlobalGrid();

        //DrawGrid
        int columns     = grid.getNbColumns();
        int rows        = grid.getNbRows();
        int cellSize    = grid.getCellSize();
        int width       = (int) environment.getTopology().getDimensions().getWidth();
        int height      = (int) environment.getTopology().getDimensions().getHeight();

        int i,j,x,y;
        Color oldColor  = graphics.getColor();
        graphics.setColor(Color.lightGray);

        for(i=0; i <= columns; i++) {
            x = i*cellSize;
            graphics.drawLine(x, 0, x, height);
        }

        for(i=0; i <= rows; i++) {
            y = i*cellSize;
            graphics.drawLine(0, y, width, y);
        }

        PheromoneCell cell;

        if(showPheromone) {
            //Draw Pheromone Quantities
            graphics.setColor(phColor);
            float phQ;

            if(nodeMapToDisplay != null) {
                graphics.setColor(nodeMapToDisplay.getColor());
                grid = nodeMapToDisplay.getPheromoneGrid();
            }

            for (i = 0; i < rows; i++) {
                for (j = 0; j < columns; j++) {
                    cell = grid.getCell(j, i);
                    phQ = cell.getPhQuantity();
                    if (phQ > 0) {
                        if(displayPheromoneAsNumeric) {
                            int dphQ = (int) Math.abs(phQ * 100);
                            char[] chars = Integer.toString(dphQ).toCharArray();
                            graphics.drawChars(chars, 0, chars.length, (int) cell.getCenter().getX() - 5, (int) cell.getCenter().getY() + 5);
                        } else {
                            paintPheromone(graphics, cellSize, cell, graphics.getColor());

                        }
                    }
                }
            }

        }

        //Draw Nodes Map awareness
        //Garder à l'esprit que le niveau de pheromone affiché
        //est le niveau "réel" et non celui de chaque node
        if(environment.isColoredPheromone()) {
            PheromoneGrid localGrid = null;
            Color color = null;
            Color scolor = null;
            int cX, cY;
            int squareWidth = 3;

            resetCountNodeAware();
            java.util.List<EvapNode> list = environment.getEvapNodeList();

            for(EvapNode e : list) {
                localGrid = e.getPheromoneGrid();
                graphics.setColor(e.getColor());

                for (i = 0; i < rows; i++) {
                    for (j = 0; j < columns; j++) {
                        cell = localGrid.getCell(j,i);
                        if( cell.getPhQuantity() > 0) {
                            cX = j * cellSize+1;
                            cY = i * cellSize+1;
                            graphics.fillRect(cX + squareWidth*countNodePhAware[j][i], cY, squareWidth, squareWidth);
                            countNodePhAware[j][i]++;
                        }
                    }
                }
            }
        } //End Draw Nodes Map awareness

        graphics.setColor(oldColor);
    }

    protected void paintPheromone(Graphics graphics, int cellSize, PheromoneCell cell, Color baseColor) {
        float phQ = cell.getPhQuantity();
        if(phQ > 0) {
            Color color = new Color(baseColor.getRed(), baseColor.getGreen(), baseColor.getBlue(), (int)Math.ceil((float)phQ * phAlphaMax));
            graphics.setColor(color);
            graphics.fillRect(cell.getColumn()*cellSize, cell.getRow() * cellSize, cellSize, cellSize);
        }
    }

    public void resetCountNodeAware() {
        PheromoneGrid grid = environment.getGlobalGrid();
        int columns     = grid.getNbColumns();
        int rows        = grid.getNbRows();

        for(int i=0; i<rows;i++) {
            for(int j=0; j<columns; j++) {
                countNodePhAware[j][i] = 0;
            }
        }
    }

    public EvapEnvironment getEnvironment() {
        return environment;
    }

    public boolean isShowPheromone() {
        return showPheromone;
    }

    public void setShowPheromone(boolean showPheromone) {
        this.showPheromone = showPheromone;
    }

    public boolean isDisplayPheromoneAsNumeric() {
        return displayPheromoneAsNumeric;
    }

    public void setDisplayPheromoneAsNumeric(boolean displayPheromoneAsNumeric) {
        this.displayPheromoneAsNumeric = displayPheromoneAsNumeric;
    }

    public Color getPhColor() {
        return phColor;
    }

    public void setPhColor(Color phColor) {
        this.phColor = phColor;
    }

    @Override
    public void onSelection(Node node) {
        if(node instanceof EvapNode) {
            if(environment.getEvapNodeList().contains(node)) {
                if(nodeMapToDisplay == node) {
                    nodeMapToDisplay = null;
                } else {
                    nodeMapToDisplay = (EvapNode)node;
                }
            }
        }
        jTopology.updateUI();
    }
}
