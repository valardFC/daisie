package jbotsim.evap.ui;

import jbotsim.evap.EvapEnvironment;
import jbotsim.ui.CommandListener;
import jbotsim.ui.JTopology;

import java.awt.*;

/**
 * Created by vklein on 12/03/15.
 */
public class JEvapTopology extends JTopology implements CommandListener {

    private static final String CMD_SHOWHIDE_LOCAL_MAPS = "Show/Hide local maps";
    private static final String CMD_SHOWHIDE_PHEROMONE = "Show/Hide Pheromones";
    private static final String CMD_DISPLAYMODE_PHEROMONE = "Switch pheromones display mode";
    private static final String CMD_DISPLAYMODE_TERRITORIES = "Territory display mode";

    EvapDisplayHelper evapDisplay = null;
    private EvapEnvironment environment;

    public JEvapTopology(EvapEnvironment environment) {
        super(environment.getTopology());
        this.environment = environment;
        this.evapDisplay = new EvapDisplayHelper(this, environment);
        this.addCommand(CMD_SHOWHIDE_LOCAL_MAPS);
        this.addCommand(CMD_SHOWHIDE_PHEROMONE);
        this.addCommand(CMD_DISPLAYMODE_PHEROMONE);
        this.addCommand(CMD_DISPLAYMODE_TERRITORIES);
        this.addCommandListener(this);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        evapDisplay.paint(graphics);
    }

    public EvapEnvironment getEnvironment() {
        return evapDisplay.getEnvironment();
    }

    public boolean isShowPheromone() {
        return evapDisplay.isShowPheromone();
    }

    public void setShowPheromone(boolean showPheromone) {
        evapDisplay.setShowPheromone(showPheromone);
    }

    public boolean isDisplayPheromoneAsNumeric() {
        return evapDisplay.isDisplayPheromoneAsNumeric();
    }

    public void setDisplayPheromoneAsNumeric(boolean displayPheromoneAsNumeric) {
        evapDisplay.setDisplayPheromoneAsNumeric(displayPheromoneAsNumeric);
    }

    @Override
    public void onCommand(String cmd) {
        System.out.println("Hey " + cmd);
        if(CMD_SHOWHIDE_LOCAL_MAPS.equals(cmd)) {
            environment.setColoredPheromone(!environment.isColoredPheromone());
        } else if(CMD_SHOWHIDE_PHEROMONE.equals(cmd)) {
            setShowPheromone(!isShowPheromone());
        } else if(CMD_DISPLAYMODE_PHEROMONE.equals(cmd)) {
            setDisplayPheromoneAsNumeric(!isDisplayPheromoneAsNumeric());
        } else if(CMD_DISPLAYMODE_TERRITORIES.equals(cmd)) {
            setShowPheromone(false);
        }
        updateUI();
    }
}
