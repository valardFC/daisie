package jbotsim.evap;

import java.awt.*;

/**
 * Created by vklein on 06/03/15.
 */
public class PheromoneCell implements Cloneable {

    static final float PHEROMONE_MAX_QUANTITY = 1;
    static final float PHEROMONE_MIN_QUANTITY = 0;

    //Propriétés
    private int column; //Cell column
    private int row; //Cell row
    /**
     * Pheromone Quantity
     */
    private float phQuantity;
    private Point center = null;

    /**
     * @param column
     * @param row
     */
    public PheromoneCell(int column, int row) {
        this.column = column;
        this.row = row;
        this.phQuantity = 0;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        PheromoneCell clone = (PheromoneCell) super.clone();
        clone.center = (Point) this.center.clone();
        return clone;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public float getPhQuantity() {
        return phQuantity;
    }

    public void setPhQuantity(float phQuantity) {
        this.phQuantity = phQuantity;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void evaporatePheromone(float evapStep) {
        this.phQuantity -= evapStep;

        if(this.phQuantity < PHEROMONE_MIN_QUANTITY) {
            this.phQuantity = PHEROMONE_MIN_QUANTITY;
        }
    }
}
