package jbotsim.evap;

import jbotsim.Link;
import jbotsim.Message;
import jbotsim.Node;
import jbotsim.evap.event.PheromoneListener;
import jbotsim.utils.NeighbourStep;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by vklein on 09/03/15.
 */
public class EvapNode extends Node {

    public static final double DEFAULT_SPEED = 3;

    /*
     *Ce type de node doit  :
     * - être orienté = avoir une notion de son avant et de son arriere (Eventuellement)
     * - Pouvoir déposer une phéromone
     * - echange sa carte de pheromones avec d'autres EvapNode
     */
    private static Random random = new Random();

/*==================================================================================================================*
 *                      BUSINESS PROPERTIES
 *==================================================================================================================*/
    private final EventListenerList listeners = new EventListenerList();
    //local pheromone map
    private PheromoneGrid pheromoneGrid = null;
    private PheromoneCell currentCell = null;
    //Cellule cible vers laquelle on veux aller
    private PheromoneCell destinationCell = null;
    private float lastDestPhQuantity = 0;
    List<PheromoneCell> neighbourhood = null;

    private double maxMove = DEFAULT_SPEED;
//*=================================================================================================================*

    public void init(PheromoneGrid pheromoneGrid) {
        this.pheromoneGrid = pheromoneGrid;
        this.neighbourhood = new ArrayList<PheromoneCell>();
        refreshCurrentCell();
        refreshNeighbourhood();
    }

    /*
         * Compare la quantité de pheromones de chaque cellule voisine, selectionne la cellule avec
         * la plus petite quantité. Si plusieurs cellules ont la même quantité en choisi une aléatoirement
         * TODO A refaire en désactivant les voisins qui sont hors des capacités de mobilité de la node.
         * TODO voir à quel niveau on tire une exception si res = null
         */
    private void selectDestinationCell(){
        PheromoneCell destCell = null;
        float minQuantity = PheromoneCell.PHEROMONE_MAX_QUANTITY; //quantité minimale trouvée parmis les voisins
        List<PheromoneCell> cellsSelected = new ArrayList<PheromoneCell>();

        for(PheromoneCell pCell : neighbourhood) {
            float pQuantity = pCell.getPhQuantity();

            if(pQuantity < minQuantity) {
                cellsSelected.clear();
                minQuantity = pQuantity;
                cellsSelected.add(pCell);
            } else if(pQuantity == minQuantity) {
                cellsSelected.add(pCell);
            }
        }

        int size = cellsSelected.size();
        if(size > 0) {
            destCell = cellsSelected.get(random.nextInt(size));
        }
        this.destinationCell = destCell;
        lastDestPhQuantity = destinationCell.getPhQuantity();
        this.setDirection(destCell.getCenter());
    }

    /**
     * Refresh the list of neighbour cells
     */
    private void refreshNeighbourhood() {
        int cx = this.currentCell.getColumn();
        int cy = this.currentCell.getRow();

        neighbourhood.clear();

        // For each theoric neighbour cell, check if it exist
        for(NeighbourStep e: NeighbourStep.values())  {
            if(cellExist(cx+e.getXstep(), cy+e.getYstep()))
                neighbourhood.add(pheromoneGrid.getCell(cx + e.getXstep(), cy + e.getYstep()));
        }
    }

    /**
     * Return true if a cell line y, column x exist in the pheromoneGrid
     * @param x
     * @param y
     * @return
     */
    private boolean cellExist(int x, int y) {
        boolean res = true;

        if(x < 0 || y < 0 || x >= pheromoneGrid.getNbColumns() || y >= pheromoneGrid.getNbRows() ) {
            res = false;
        }

        return res;
    }

    /**
     * Drop the max quantity of pheromone in the currentCell
     */
    private void dropPheromone() {
        currentCell.setPhQuantity(PheromoneCell.PHEROMONE_MAX_QUANTITY);
        fireDropPheromone(currentCell, PheromoneCell.PHEROMONE_MAX_QUANTITY);
    }

    /**
     * Règle : on conserve la valeur la plus élevée en phéromone pour
     * une cellule donnée. La valeur la plus élevée étant necessairement une information
     * plus récente.
     * @param pGridToMerge
     */
    public void mergePheromoneMap(PheromoneGrid pGridToMerge) {
        //pre
        if(pGridToMerge==null)
            return;

        PheromoneCell localCell = null;
        PheromoneCell externCell = null;
        for(int i = 0; i < this.pheromoneGrid.getNbRows(); i++) {
            for(int j = 0; j < this.pheromoneGrid.getNbColumns(); j++) {
                localCell = this.pheromoneGrid.getCell(j,i);
                externCell = pGridToMerge.getCell(j,i);
                if(externCell.getPhQuantity() > localCell.getPhQuantity()) {
                    localCell.setPhQuantity(externCell.getPhQuantity());
                }
            }
        }
    }

    /*================================================================================================================
                                            Event management
     ================================================================================================================*/

    @Override
    public void onMessage(Message message) {
        Object content = message.getContent();
        if (content instanceof PheromoneGrid) {
            Link link = message.getSender().getCommonLinkWith(this);
            if(link != null) {
                mergePheromoneMap((PheromoneGrid) content);
                if (destinationCell != null && lastDestPhQuantity < destinationCell.getPhQuantity()) {
                    selectDestinationCell();
                }
            }
        }
    }

    @Override
    public void onClock() {

        //TODO Trouver une autre solution
        if(this.destinationCell == null) {
            selectDestinationCell();
        }

        double toDest  = distance(destinationCell.getCenter());
        if(toDest <= maxMove ) {
            this.move(toDest);
            //Target cell reached
            refreshCurrentCell();
            dropPheromone();
            refreshNeighbourhood();
            selectDestinationCell();
        } else {
            this.move(maxMove);
            refreshCurrentCell();
        }
        sendAll(new Message(this.pheromoneGrid));
    }

    private void refreshCurrentCell() {
        //compute row and cell from node's coordinate
        int column  = (int)Math.floor((float) this.getX() / this.pheromoneGrid.getCellSize());
        int row     = (int)Math.floor((float) this.getY() / this.pheromoneGrid.getCellSize());
        this.currentCell = this.pheromoneGrid.getCell(column, row);
    }

    //================================================================================================================

    /*================================================================================================================
                                            PheromoneListeners management
     ================================================================================================================*/
    private PheromoneListener[] getPheromoneListeners() {
        return listeners.getListeners(PheromoneListener.class);
    }

    public void addPheromoneListener(PheromoneListener listener) {
        listeners.add(PheromoneListener.class, listener);
    }

    private void fireDropPheromone(PheromoneCell cell, float phQuantity) {
        for(PheromoneListener l: getPheromoneListeners() ) {
            l.onPheromoneDrop(cell, phQuantity);
        }
    }
    //*===============================================================================================================

    /*================================================================================================================
                                           Getter and Setter
    ================================================================================================================*/
    public PheromoneCell getCurrentCell() {
        return currentCell;
    }

    public void setCurrentCell(PheromoneCell currentCell) {
        this.currentCell = currentCell;
    }

    public PheromoneCell getDestinationCell() {
        return destinationCell;
    }

    public void setDestinationCell(PheromoneCell destinationCell) {
        this.destinationCell = destinationCell;
    }

    public double getMaxMove() {
        return maxMove;
    }

    public void setMaxMove(double maxMove) {
        this.maxMove = maxMove;
    }

    public PheromoneGrid getPheromoneGrid() {
        return pheromoneGrid;
    }
    //*===============================================================================================================
}
