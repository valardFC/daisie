package jbotsim.evap;

/**
 * Created by vklein on 12/03/15.
 */
public class ShareMapContent {
    private PheromoneGrid grid;

    public ShareMapContent(PheromoneGrid grid) {
        this.grid = grid;
    }

    public PheromoneGrid getGrid() {
        return grid;
    }
}
