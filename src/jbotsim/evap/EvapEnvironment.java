package jbotsim.evap;

import jbotsim.Node;
import jbotsim.Topology;
import jbotsim.evap.event.PheromoneListener;
import jbotsim.event.ClockListener;
import jbotsim.event.TopologyListener;
import jbotsim.utils.ColorUtils;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by vklein on 10/03/15.
 */
public class EvapEnvironment implements PheromoneListener, TopologyListener, ClockListener {

    private PheromoneGrid globalGrid = null;
    private PheromoneGrid modelGrid = null;
    private Topology topology = null;
    private List<EvapNode> evapNodeList = new ArrayList<EvapNode>();
    private boolean coloredPheromone = false;
    List<Color> lstColor = new ArrayList<Color>();
    Iterator<Color> icolor = null;

    /**
     * @param cellSize the length of the square side of a cell.
     * @param topology
     */
    public EvapEnvironment(int cellSize, Topology topology) {
        this.topology = topology;
        int width = (int)topology.getDimensions().getWidth();
        int height = (int)topology.getDimensions().getHeight();
        this.globalGrid = new PheromoneGrid(cellSize, width, height);
        try {
            this.modelGrid = (PheromoneGrid) this.globalGrid.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this.topology.addTopologyListener(this);
        this.topology.addClockListener(this);

        //Temp colors management
        lstColor.add(Color.RED);
        lstColor.add(Color.GREEN);
        lstColor.add(Color.YELLOW);
        lstColor.add(Color.BLUE);
        lstColor.add(Color.WHITE);
        lstColor.add(Color.ORANGE);
        lstColor.add(Color.MAGENTA);
        lstColor.add(Color.BLACK);
        icolor = lstColor.iterator();
    }

    public EvapEnvironment(int cellSize, Topology topology, float evaporation) {
        this(cellSize, topology);
        PheromoneGrid.setEvaporation(evaporation);
    }

    @Override
    public void onPheromoneDrop(PheromoneCell cell, float phQuantity) {
        //Update of the global grid
        PheromoneCell envCell = globalGrid.getCell(cell.getColumn(), cell.getRow());
        envCell.setPhQuantity(phQuantity);
    }

    @Override
    public void onNodeAdded(Node node) {
        if(node instanceof  EvapNode) {
            EvapNode enode = (EvapNode) node;

            try {
                PheromoneGrid nodeGrid = (PheromoneGrid) modelGrid.clone();
                enode.init(nodeGrid);

                //Environment listen to node pheromone drops
                enode.addPheromoneListener(this);
                if(isColoredPheromone()) {
                    Color scolor = ColorUtils.getRandomHSBColor();
                    if(icolor.hasNext()) {
                        scolor = icolor.next();
                    }
                    enode.setColor(scolor);
                }
                evapNodeList.add(enode);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNodeRemoved(Node node) {
        if(node instanceof EvapNode) {
            EvapNode enode = (EvapNode) node;
            evapNodeList.remove(enode);
        }
    }

    @Override
    public void onClock() {
        //Manage pheromone evaporation
        globalGrid.evaporate();
        for(EvapNode e : evapNodeList) {
            e.getPheromoneGrid().evaporate();
        }
    }

    public Topology getTopology() {
        return topology;
    }

    public PheromoneGrid getGlobalGrid() {
        return globalGrid;
    }

    public PheromoneGrid getModelGrid() {
        return modelGrid;
    }

    public List<EvapNode> getEvapNodeList() {
        return evapNodeList;
    }

    public boolean isColoredPheromone() {
        return coloredPheromone;
    }

    public void setColoredPheromone(boolean coloredPheromone) {
        this.coloredPheromone = coloredPheromone;
        if(coloredPheromone) {
            Color color = null;
            icolor = lstColor.iterator();
            for(EvapNode e: evapNodeList) {
                if(icolor.hasNext()) {
                    color = icolor.next();
                } else {
                    color = ColorUtils.getRandomHSBColor();
                }
                e.setColor(color);
            }
        } else {
            for(EvapNode e: evapNodeList) {
                e.setColor(Color.GRAY);
            }
        }
    }

}
